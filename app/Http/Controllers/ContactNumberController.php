<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactNumber;
use Illuminate\Http\Response;

class ContactNumberController extends Controller
{
    public function getAll() {
        return ContactNumber::all();
    }


    public function getById($user_id) {
        return ContactNumber::find($user_id);
    }

    public function getAllByUser($user_id) {
        return ContactNumber::where('user_id', '=', $user_id)
               ->get();
    }

    public function create(Request $request, $user_id) {
        return ContactNumber::create([
            'user_id' => $user_id,
            'contact_number' => $request->contact_number
        ]);
    }

    public function remove($id) {
        $contactNumber = ContactNumber::find($id);

        if ($contactNumber) {
            $contactNumber->delete();
            return;
        }

        return Response::create('No result was found', 404);

    }

    public function removeByUser($user_id) {
        ContactNumber::where('user_id', $user_id)->delete();
        return;
    }
}
