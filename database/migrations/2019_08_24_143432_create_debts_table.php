<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDebtsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('amount');
            $table->bigInteger('lender_id')->unsigned();
            $table->bigInteger('receiver_id')->unsigned();
            $table->string('note');
            $table->dateTime('transaction_date');
            $table->timestamps();

            $table->foreign('lender_id')
                  ->references('id')->on('users')
                  ->onDelete('restrict');

            $table->foreign('receiver_id')
                ->references('id')->on('users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debts');
    }
}
