<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/users/', 'Auth\RegisterController@create');

Route::post('/login/', 'Auth\RegisterController@login');

Route::post('/logout/', 'Auth\RegisterController@logout');

Route::get('/contact-numbers/', 'ContactNumberController@getAll');;

Route::post('/users/{user_id}/contact-numbers/', 'ContactNumberController@create');

Route::get('/users/{user_id}/contact-numbers/', 'ContactNumberController@getAllByUser');

Route::delete('/contact-numbers/{id}/', 'ContactNumberController@remove');

Route::delete('/users/{user_id}/contact-numbers', 'ContactNumberController@removeByUser');